﻿using UnityEngine;
using System.Collections;

public class lookAtRotation : MonoBehaviour {

	public Transform target;

	void Update(){
		Vector3 relativePos = target.position - transform.position;
		Quaternion rotation = Quaternion.LookRotation (relativePos);
		Quaternion obj = Quaternion.Euler (transform.rotation.x,rotation.eulerAngles.y+90,transform.rotation.z);
		transform.rotation = obj;
	}
}
