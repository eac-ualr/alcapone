﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;


public class takePicture1 : MonoBehaviour {

	public AudioClip cameraFx;

	public AudioSource audio;
	private Image imagenButton;

	public GameObject buttonClose;
	public GameObject buttonTakePicture;
	public GameObject buttonSwichCam;
	public GameObject buttonHelp;
	private string url;
	private string path;

	public Canvas canvasIntro;

	private Image screenshot;
	public GameObject picture;
	
	void Start(){
		imagenButton = buttonTakePicture.GetComponent<Image> ();
		buttonClose.SetActive (false);
		screenshot = picture.GetComponent<Image> ();
		picture.SetActive (false);

	}

	void OnEnable ()
	{
		// call backs
		ScreenshotManager.OnScreenshotTaken += ScreenshotTaken;
		ScreenshotManager.OnScreenshotSaved += ScreenshotSaved;	
		ScreenshotManager.OnImageSaved += ImageSaved;
	}

	void OnDisable ()
	{
		ScreenshotManager.OnScreenshotTaken -= ScreenshotTaken;
		ScreenshotManager.OnScreenshotSaved -= ScreenshotSaved;	
		ScreenshotManager.OnImageSaved -= ImageSaved;
	}

	public void OnSaveScreenshotPress()
	{
		
		buttonTakePicture.SetActive (false);
		buttonSwichCam.SetActive (false);
		buttonHelp.SetActive (false);
		audio.PlayOneShot (cameraFx);
		ScreenshotManager.SaveScreenshot("TrojansPic", "TrojansApp", "jpeg");

	}

	public void OnSaveImagePress()
	{
		//ScreenshotManager.SaveImage(texture, "MyImage", "png");
	}

	void ScreenshotTaken(Texture2D image)
	{
		//console.text += "\nScreenshot has been taken and is now saving...";
		screenshot.sprite = Sprite.Create(image, new Rect(0, 0, image.width, image.height), new Vector2(.5f, .5f));
		screenshot.color = Color.white;
	}

	void ScreenshotSaved(string path)
	{
		picture.SetActive (true);
		buttonClose.SetActive (true);
	}

	void ImageSaved(string path)
	{
		//console.text += "\n" + texture.name + " finished saving to " + path;
	}


	public void closePicture (){
		picture.SetActive (false);
		buttonClose.SetActive (false);
		buttonTakePicture.SetActive (true);
		buttonSwichCam.SetActive (true);
		buttonHelp.SetActive (true);

	}

	public void hideIntro(){
		canvasIntro.enabled = false;
	}
		
}
