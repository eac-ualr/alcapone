﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class controlButtons : MonoBehaviour {


	public float Zmin;
	public float Zmax;

	public float Xmin;
	public float Xmax;

	public GameObject fwd;
	public GameObject bwd;
	public GameObject left;
	public GameObject right;

	// Update is called once per frame
	void Update () {
		
		if (fwd.transform.position.z > Zmax) 
			fwd.SetActive (false);	
		else
			fwd.SetActive (true);

		if (bwd.transform.position.z < Zmin) 
			bwd.SetActive (false);	
		else
			bwd.SetActive (true);

		if (left.transform.position.x < Xmin) 
			left.SetActive (false);	
		else
			left.SetActive (true);
		
		if (right.transform.position.x > Xmax) 
			right.SetActive (false);	
		else
			right.SetActive (true);
	}
}
