﻿using UnityEngine;
using System.Collections;

public class GoalHandler : MonoBehaviour {

    public SwipeThrow swipeThrow;
	
	void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            swipeThrow.goal = 1;
			swipeThrow.playSound(Random.Range(19,21));
			swipeThrow.playSound(Random.Range(7,9));
        }

    }
}
