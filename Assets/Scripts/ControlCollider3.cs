﻿using UnityEngine;
using System.Collections;

public class ControlCollider3 : MonoBehaviour {

    public SwipeThrow swipeThrow;

	void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            swipeThrow.goal = 4;
			swipeThrow.playSound(Random.Range(0,2));
        }

    }
}
