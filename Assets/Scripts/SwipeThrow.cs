﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class SwipeThrow : MonoBehaviour
{

    public GameObject Player;
    public GameObject target;
	public GameObject triggerGoal;
    public Transform shootPosition;
    public GameObject Ball;
    public float throwScaler = 1.8f;
    public float averageSwipePercentage = .65f;
    public float averageSwipeDuration = .15f;
	public int goal = -1;
	public bool inMotion = false;
    //The position where the swipe begins (when the swipe starts moving up for the last time)
    private Vector2 touchBegin;
    //where the swipe ends
    private Vector2 touchEnd;
    //Time where the swipe begins
    private float startTime;
    //The tim when the swipe ends.
    private float endTime;
    //The time the finger leaves the screen. Used to reset ball.
    private float releaseTime;
    //The velocity of the swipe
    private float touchVelocity;
    //The position of the touch last frame, to find the bottom of the swipe.
    private Vector3 previousPosition;
    //Storing whether the ball is reset (reset = true) or in motion (rest = false)
    private bool Reset = true;

	public GameObject swip;
	public GameObject AR;

	public Image img;

	public Sprite[] sprites;
	private float timerMessage = 0;
	private bool activeTimer = false;

	public GameObject playerGroup1;
	public GameObject playerGroup2;
	private bool score = false;
	public Animator anim;
	public GameObject coin;
	public Transform parentCoint;
	public GameObject closeButton; 
	public AudioSource audio;
	public AudioClip[] sounds;
	public followListenerSound sourceSound;
	public bool startTimer;

	public GameObject AutoShootBtn;




	private float initialAngle;

	private Rigidbody rigid;
	private Vector3 finalVelocity;
	private float offSet;

	private int numShoots;
	//public RectTransform frame;

	void Start(){
		playerGroup2.SetActive (false);
		coin.SetActive (false);
		startTimer = false;
		offSet = 0.5f;
		numShoots = 0;
		AutoShootBtn.SetActive (false);
	}

    void Update()
    {
        

		if (activeTimer) {
			if (goal == 1) {
				img.sprite = sprites [1];
				activeTimer = false;
				score = true;
				closeButton.SetActive (false);
				AutoShootBtn.SetActive (false);
				numShoots = 0;
			}
		}

		if (Time.time > timerMessage && activeTimer) {
			if (goal > 0)
				img.sprite = sprites [goal];
			
			inMotion = true;
			activeTimer = false;
			if (numShoots > 3) {
				AutoShootBtn.SetActive (true);
			}
			//print ("InMotion2"+inMotion);
		}

		if (startTimer) {
			startTimer = false;
			timerMessage = Time.time + 2;
			activeTimer = true;
			//print ("InMotion"+inMotion);
		}

		if (inMotion) {
			//If it has been 4 seconds since the throw and they have not made a goal, reset the ball. 
			if (GetTimeSinceThrown () > 4 && Reset == false) { //&& goal != true
				BallReset ();
			}

			if (GetTimeSinceThrown () > 10) {
				if (Reset != true) {
					BallReset ();
				}
				releaseTime = Time.time;
			}
		}

#if UNITY_EDITOR
        if (Reset == true)
        {
			if(inMotion){
	            if (Input.GetMouseButtonDown(0) == true)
	            {
	               
	                touchBegin = Input.mousePosition;
	                startTime = Time.time;
	                previousPosition = Input.mousePosition;
	    	    }

	            if (Input.GetMouseButton(0) == true)
	            {
	                //slider.value = GetPower();
					//frame.localPosition = new Vector3(0,Screen.height* 0.4f * GetPower(),0);
	                if (Input.mousePosition.y < previousPosition.y)
	                {
	                    previousPosition = Input.mousePosition;
	                    touchBegin = Input.mousePosition;
	                    startTime = Time.time;
	                }
	                else
	                {
	                    previousPosition = Input.mousePosition;
	                }
	            }

	            if (Input.GetMouseButtonUp(0) == true)
	            {
	                releaseTime = Time.time;
	                touchEnd = Input.mousePosition;
	                endTime = Time.time;
	                touchVelocity = GetVelocity();
	                //slider.value = GetPower();
					//frame.localPosition = new Vector3(0,Screen.height* 0.4f * GetPower(),0);

					FireBallCustom(touchVelocity);
					activeTimer = true;
	                //if (GetPower() > .7 && GetPower() < 1.3)
	                //{
	                    //AutoShoot();
						;
	               // }
	               // else
	               // {
						
						//Ball.GetComponent<Rigidbody>().useGravity = true;
	                    //Ball.GetComponent<Rigidbody>().AddForce(GetSwipeVector() * (touchVelocity * Mathf.Sqrt(GetGoalDistance() / throwScaler)), ForceMode.VelocityChange);
	               // }
	                touchBegin = Vector2.zero;
	                touchEnd = Vector2.zero;
	                startTime = 0;
	                endTime = 0;
	                Reset = false;
					//inMotion = false;
					timerMessage = Time.time + 2.5f;
					activeTimer = true;
	            }
			}
        }
#endif

#if UNITY_ANDROID || UNITY_IOS
        if (Reset == true)
        {
			if(inMotion){
	            foreach (Touch item in Input.touches)
	            {
	                if (item.phase == TouchPhase.Began)
	                {
				
	                    touchBegin = item.position;
	                    startTime = Time.time;
	                    previousPosition = item.position;
	                }

	                if (item.phase == TouchPhase.Moved || item.phase == TouchPhase.Stationary)
	                {
	                    //Calculate the power of the touch with 1 being best power to make a goal. 
	                    //slider.value = GetPower();
						//frame.localPosition = new Vector3(0,Screen.height* 0.4f * GetPower(),0);
	                    //If the new posiiton is lower than previous position, reset the swipe. Otherwise update previousPosition.
	                    if (item.position.y < previousPosition.y)
	                    {
	                        previousPosition = item.position;
	                        touchBegin = item.position;
	                        startTime = Time.time;
	                    }
	                    else
	                    {
	                        previousPosition = item.position;
	                    }
	                }
	                //When the touch ends, set release and end touch values.
	                if (item.phase == TouchPhase.Ended || item.phase == TouchPhase.Canceled)
	                {
						
						releaseTime = Time.time;
	                    touchEnd = item.position;
	                    endTime = Time.time;
	                    touchVelocity = GetVelocity();
	                    //slider.value = GetPower();
						//frame.localPosition = new Vector3(0,Screen.height* 0.4f * GetPower(),0);
	                    //See if the power is close enough to use an autoshoot.(still dependent on swipe angle)
						FireBallCustom(touchVelocity);
						activeTimer = true;

						/*
						if (GetPower() > 0.7f && GetPower() < 1.3f)
	                    {
	                        
							FireBall();
							activeTimer = true;
	                    }
	                    //If it's not, only use their input to throw ball. 
						else 
	                    {
	                       
	                        Ball.GetComponent<Rigidbody>().useGravity = true;
	                        Ball.GetComponent<Rigidbody>().AddForce(GetSwipeVector() * (touchVelocity * Mathf.Sqrt(GetGoalDistance() / throwScaler)), ForceMode.VelocityChange);
							activeTimer = true;
	                    }
						*/
	                    //Reset throw values.
	                    touchBegin = Vector2.zero;
	                    touchEnd = Vector2.zero;
	                    startTime = 0;
	                    endTime = 0;
	                    Reset = false;

						timerMessage = Time.time + 2.5f;
					
	                }
	            }
			}
        }
        #endif        
    }


    //Return the current Input divided by the optimum input. The closer the 1, the better the throw.
    public float GetPower()
    {
        return ((GetVelocity() * Mathf.Sqrt(GetGoalDistance() / throwScaler)) / ((averageSwipePercentage / averageSwipeDuration) * Mathf.Sqrt(GetGoalDistance() / throwScaler)));
    }

    //Return the distance between the player and the goal. 
    public float GetGoalDistance()
    {
		return Vector3.Distance(Player.transform.position, target.transform.position);
    }

    //Return the velocity of the swipe. 
    public float GetVelocity()
    {
        if(GetDuration() != 0)
        {
            return (Vector2.Distance(touchBegin, touchEnd) / Screen.height) / GetDuration();
        }
        else
        {
            return 0f;
        }
        
    }

    //return the duration of the swipe. 
    public float GetDuration()
    { 
        return endTime - startTime;
    }

    //Return the direction of swipe.
    public Vector3 GetSwipeVector()
    {
       return Ball.transform.forward-new Vector3(0,0, (touchBegin - touchEnd).normalized.x/2);
    }

    //Return the time that has passed since the ball was released.
    public float GetTimeSinceThrown()
    {
        return Time.time - releaseTime;
    }

    //Reset the ball to starting state. 
    public void BallReset()
    {
		
		if (score) {
			sourceSound.changeListener (2);
			playerGroup2.SetActive (true);
			playerGroup1.SetActive (false);
			audio.PlayOneShot (sounds[22]);
			anim.SetTrigger ("Play");

		} else {
			releaseTime = Time.time;
       		Ball.GetComponent<Rigidbody> ().useGravity = false;
			Ball.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			Ball.GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
			Ball.transform.position = shootPosition.position;
			Ball.transform.rotation = shootPosition.rotation;
			previousPosition = new Vector3 (1000000000, 0, 0);
			goal = -1;
			img.sprite = sprites [0];
			Reset = true;
		}

		score = false;
    }

	public void AutoShootBtnReset(){
		AutoShootBtn.SetActive(false);
		numShoots = 0;
	}

    //Shoot the ball to get it very close to make a basket. 
    public void AutoShoot() {

		releaseTime = Time.time;
		FireBallCustom (-1001);
		activeTimer = true;
		touchBegin = Vector2.zero;
		touchEnd = Vector2.zero;
		startTime = 0;
		endTime = 0;
		Reset = false;
		timerMessage = Time.time + 2.5f;
    }


   

	public void reset(){
		goal = -1;
		audio.PlayOneShot (sounds[3]);
		sourceSound.changeListener(0);
		inMotion = false;
		AR.SetActive (true);
		swip.SetActive(false);

	}
	 
	public void EndAnimation(){
		coin.transform.parent = parentCoint;
		coin.SetActive (true);
		closeButton.SetActive (true);
		playerGroup1.SetActive (true);
		playerGroup2.SetActive (false);
		reset ();
		score = false;
	}

	public void playSound(int num){
		if(num < sounds.Length)
			audio.PlayOneShot (sounds[num]);
	}



	public void FireBallCustom(float vel){


		numShoots++;

		rigid = Ball.GetComponent<Rigidbody>();
		rigid.useGravity = true;

		Vector3 p = target.transform.position;

		float gravity = Physics.gravity.magnitude;


		// Positions of this object and the target on the same plane
		Vector3 planarTarget = new Vector3(p.x, 0, p.z);
		Vector3 planarPostion = new Vector3(Ball.transform.position.x, 0, Ball.transform.position.z);

		// Planar distance between objects
		float distance = Vector3.Distance(planarTarget, planarPostion);
		//print ("distance " + distance);

		if ((vel < distance - offSet || vel > distance + offSet) && vel> -1000)
			distance = vel;
		
		if (distance > 5.57f)
			initialAngle = 45;
		else
			initialAngle = 90 - (45 * distance / 5.57f);

		// Selected angle in radians
		float angle = initialAngle * Mathf.Deg2Rad;

		//print ("initialAngle " + initialAngle);


		// Distance along the y axis between objects
		float yOffset = Ball.transform.position.y - p.y;

		float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yOffset));

		Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));

		// Rotate our velocity to match the direction between the two objects
		float angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPostion);
		if(planarTarget.x > planarPostion.x)
			finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;
		else
			finalVelocity = Quaternion.AngleAxis(-angleBetweenObjects, Vector3.up) * velocity;

		rigid.velocity = finalVelocity;

		// Alternative way:
		// rigid.AddForce(finalVelocity * rigid.mass, ForceMode.Impulse);

	}

}


