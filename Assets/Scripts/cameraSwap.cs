﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class cameraSwap : MonoBehaviour 
{
    private VuforiaAbstractBehaviour mQCAR;
	public GameObject[] particles;



    void Start() {
		mQCAR = (VuforiaAbstractBehaviour)FindObjectOfType(typeof(VuforiaAbstractBehaviour));


    }
 
    public void swapCamera()
    {
        
        CameraDevice.CameraDirection currentDir = CameraDevice.Instance.GetCameraDirection();

		if (currentDir == CameraDevice.CameraDirection.CAMERA_BACK || currentDir == CameraDevice.CameraDirection.CAMERA_DEFAULT) {
			
			RestartCamera (CameraDevice.CameraDirection.CAMERA_FRONT, true);
		} else { 
			RestartCamera (CameraDevice.CameraDirection.CAMERA_BACK, false);

		}

		foreach (GameObject p in particles) {
			p.transform.localScale = new Vector3(p.transform.localScale.x,p.transform.localScale.y,p.transform.localScale.z * -1);
		}
    }
 
    private void RestartCamera(CameraDevice.CameraDirection newDir, bool mirror)
    {
        CameraDevice.Instance.Stop();
        CameraDevice.Instance.Deinit();
 
        CameraDevice.Instance.Init(newDir);
 
        // Set mirroring 
		var config = VuforiaRenderer.Instance.GetVideoBackgroundConfig();
	
		config.reflection = mirror ? VuforiaRenderer.VideoBackgroundReflection.ON : VuforiaRenderer.VideoBackgroundReflection.OFF;
		VuforiaRenderer.Instance.SetVideoBackgroundConfig(config);
		#if UNITY_ANDROID || UNITY_IOS
        CameraDevice.Instance.Start();
		#endif
    }
}