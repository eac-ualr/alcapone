﻿using UnityEngine;
using System.Collections;

public class rotate : MonoBehaviour {

	public float speed = 3;

	void Update () {
		transform.Rotate (Vector3.up * Time.deltaTime* speed);
	}
}
