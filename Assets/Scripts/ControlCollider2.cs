﻿using UnityEngine;
using System.Collections;

public class ControlCollider2 : MonoBehaviour {

    public SwipeThrow swipeThrow;
	
	void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            swipeThrow.goal = 3;
			swipeThrow.playSound(17);
        }

    }
}
