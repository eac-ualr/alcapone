﻿using UnityEngine;
using System.Collections;

public class ControlCollider5 : MonoBehaviour {

    public SwipeThrow swipeThrow;
	
	void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            swipeThrow.playSound(Random.Range(13,16));
        }

    }
}
