﻿using UnityEngine;
using System.Collections;

public class ControlCollider1 : MonoBehaviour {

    public SwipeThrow swipeThrow;
	
	void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            swipeThrow.goal = 2;
			swipeThrow.playSound(Random.Range(10,12));
        }

    }
}
