﻿using UnityEngine;
using System.Collections;

public class persistObject : MonoBehaviour {

	public static persistObject control;

	void Awake(){
		if (control == null) {
			DontDestroyOnLoad (gameObject);
			control = this;
		}
		else if(control != this){
			Destroy(gameObject);
		}
	}
	
}
