﻿using UnityEngine;
using System.Collections;

public class ControlCollider6 : MonoBehaviour {

	public AnimationController animController;
	
	void OnTriggerEnter(Collider other)
    {
		
		if(other.gameObject.tag == "Ball")
        {
			animController.playSound(Random.Range(3,6));
        }

    }
}
