﻿
namespace EAC.Utility
{

// system namespaces
using System;
// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine;

public class AppExpirationDate
    : MonoBehaviour
{
    [Tooltip("If true the application expires at the specified date.")]
    public bool expireApplication;
    public int expirationDay;
    public int expirationMonth;
    public int expirationYear;

    [Tooltip("Index of scene to show when the application has expired.")]
    public int expiredSceneIndex;

    private DateTime m_expirationDate;

    #region MonoBehaviour callbacks
    protected void Awake ()
    {
        m_expirationDate = new DateTime(expirationYear, expirationMonth, expirationDay);
    }

    protected void Start ()
    {
        DateTime now = DateTime.Now;

        if (expireApplication && now >= m_expirationDate)
        {
            SceneManager.LoadScene(expiredSceneIndex);
        }
    }
    #endregion
}

} // namespace EAC.Utility
