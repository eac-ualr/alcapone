﻿using UnityEngine;
using System.Collections;

public class EasyHelper : MonoBehaviour {

    public GameObject Ball;
    private SphereCollider basketRadius;
    private bool helping = false;
    private Vector3 dir = Vector3.zero;

	void Start () {
        basketRadius = this.gameObject.GetComponent<SphereCollider>();
	}
	
	// If the ball is above the goal and inside the trigger, push it towards the basket
	void Update () {
        if (helping == true) {
            if (Ball.transform.position.y > this.transform.position.y)
            {
                dir = (this.transform.position - Ball.transform.position).normalized;
                Ball.GetComponent<Rigidbody>().AddForce(dir * (20 * Vector3.Distance(this.transform.position, Ball.transform.position)));
            }
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == Ball.gameObject)
        {
            helping = true;

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == Ball.gameObject)
        {
            helping = false;

        }
    }
}
