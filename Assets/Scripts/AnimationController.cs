﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class AnimationController : MonoBehaviour {
	//public Transform target1;
	//public Transform target2;

	public Transform[] target;
	public Transform[] root;
	public Animator[] anim;
	public SkinnedMeshRenderer[] PlayerSkin;
	public GameObject[] BasketBall;


	private float offset;
	private int state;
	private int actual;
	private int next;
	private bool GotoRight;
	private bool GotoLeft;
	private bool GotoForward;
	private bool GotoBackward;
	private bool changeIdle;
	private bool changeAny;
	private float offsetX;
	private float offsetZ;
	private float preOffsetX;
	private float preOffsetZ;
	private float timer;
	private bool activeTimer;
	private bool activeSetUp;
	public Mesh[] mesh;
	public GameObject panel;
	private bool activo = true;
	private RaycastHit hit;
	private int layerMask;
	//public GameObject fwd;
	//public GameObject bwd;
	//public GameObject left;
	//public GameObject right;
	public Material[] materials;
	private Animator animation1;
	public Animator animation2;
	public GameObject swipe;
	public GameObject AR;
	public GameObject loading;
	public GameObject coin;
	public GameObject character;
	public GameObject players;
	public Transform temp;
	public Transform models;
	private bool activeAnimation;
	private bool animIsplayng;
	public GameObject playerController;
	//public GameObject tutorial;
	public AudioSource audio;
	public AudioClip[] sounds;
	public followListenerSound sourceSound;
	public SwipeThrow swipeThrow;
	//public GameObject tutorial1;
	//public GameObject tutorial2;
	//public GameObject tutorial3;
	//public GameObject tutorial4;
	public bool isPause;
	public GameObject btnPlay;
	public GameObject btnPause;


	public bool targetFound;
	public GameObject[] introPages;
	private int pagesActual;
	public GameObject bgIntro;
	public GameObject btnsIntro1;
	public GameObject btnsIntro2;
	public GameObject btnsIntro3;
	public GameObject btnHelp;
	public GameObject btnLink;

	void Start () {
		
		GotoRight = false;
		GotoLeft = false;
		changeIdle = false;
		changeAny = true;
		state = 0;
		actual = 0;
		next = 1;
		timer = 0;
		activeTimer = false;
		activeSetUp = false;
		layerMask = 1 << 8;
		BasketBall[next].SetActive (false);

		swipe.SetActive (false);
		animation1 = loading.GetComponent<Animator> ();
		loading.SetActive (false);
		players.SetActive (false);
		animIsplayng = true;

		playerController.SetActive (false);
		isPause = false;
		btnPlay.SetActive (false);
		targetFound = false;
		pagesActual = 0;
		for (int i = 1; i < introPages.Length; i++) {
			introPages [i].SetActive (false);
		}
		btnHelp.SetActive (false);
		btnsIntro2.SetActive (false);
	}
		
	void Update () {

		if (Input.touchCount > 0) {
			if (Input.GetTouch (0).phase == TouchPhase.Began) {

				Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);

				if (Physics.Raycast (ray, out hit, Mathf.Infinity, layerMask)) {
					
					if (hit.transform.gameObject.name == "Coin" && changeAny) {
						
						audio.PlayOneShot (sounds [1]);
						character.transform.SetParent (temp);
						character.SetActive (false);
						players.transform.SetParent (models);
						players.SetActive (true);
						audio.PlayOneShot (sounds [2]);
						animation2.SetTrigger ("Play");
						activeAnimation = true;
						playerController.SetActive (true);
						OnTrackingActive ();
						changeAny = false;

					} else if (hit.transform.gameObject.name == "Fwd" && changeAny) {
						audio.PlayOneShot (sounds[0]);
						GotoForward = true;
					} else if (hit.transform.gameObject.name == "Bwd" && changeAny) {
						audio.PlayOneShot (sounds[0]);
						GotoBackward = true;
					} else if (hit.transform.gameObject.name == "Right" && changeAny) {
						audio.PlayOneShot (sounds[0]);
						GotoRight = true;
					} else if (hit.transform.gameObject.name == "Left" && changeAny) {
						audio.PlayOneShot (sounds[0]);
						GotoLeft = true;
					} else if (hit.transform.gameObject.name == "Circle" && animIsplayng && changeAny) {
						
						loading.SetActive (true);
						animation1.SetTrigger ("Play");
						timer = Time.time + 1.5f;
						activeTimer = true;
						animIsplayng = false;

					}
				}
			} 

			int cont = 0;
			for (int i = 0; i < Input.touchCount; i++) {
				if (Input.GetTouch (i).phase == TouchPhase.Ended)
					cont++;
			}
			if(cont == Input.touchCount) {
				animation1.SetTrigger ("Stop");
				activeTimer = false;
				loading.SetActive (false);
				animIsplayng = true;
			}
		} 


		if (Input.GetKeyUp (KeyCode.A)) {
			loading.SetActive (true);
			animation1.SetTrigger ("Play");
			timer = Time.time + 1.5f;
			activeTimer = true;
			animIsplayng = false;
		}

		if (activeTimer) {
			if (Time.time > timer) {
				activeTimer = false;
				animation1.SetTrigger ("Stop");
				sourceSound.changeListener (1);
				swipe.SetActive (true);
				swipeThrow.inMotion = true;
				swipeThrow.BallReset ();
				swipeThrow.AutoShootBtnReset ();
				//swipeThrow.startTimer = true;
				AR.SetActive (false);
				animIsplayng = true;
			}
		}
			

	
		if (Input.GetKeyUp (KeyCode.B)) {
			

			audio.PlayOneShot (sounds [1]);
			character.transform.SetParent (temp);
			character.SetActive (false);
			players.transform.SetParent (models);
			players.SetActive (true);
			audio.PlayOneShot (sounds [2]);
			animation2.SetTrigger ("Play");
			activeAnimation = true;
			playerController.SetActive (true);
			OnTrackingActive ();

		}

		float posX = Input.GetAxis ("Horizontal");
		float posY = Input.GetAxis ("Vertical");


		if (posX > 0.5 && changeAny) {
			GotoRight = true;
		} else if (posX < -0.5 && changeAny) {
			GotoLeft = true;
		}else if (posY > 0.5 && changeAny) {
			GotoForward = true;
		} else if (posY < -0.5 && changeAny) {
			GotoBackward = true;
		} else if(changeIdle){
			toIdle ();
		}


		if (GotoRight) {
			toRight ();
		} 

		if (GotoLeft) {
			toLeft ();
		} 

		if (GotoForward) {
			toForward ();
		} 

		if (GotoBackward) {
			toBackward ();
		} 

		if (changeIdle) {
			toIdle ();
		} 

	}

	void toRight (){
		switch (state) {

		case 0:

			changeAny = false;
			PlayerSkin [next].sharedMesh = mesh [0];
			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Idle"))
				anim [next].SetTrigger ("Init_Right");

			state = 1;
			changeIdle = false;
			activo = true;
			break;

		case 1:

			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Init_Right")) {
				anim [next].SetTrigger ("Right");
				state = 2;
			} 
			break;
		case 2:
			if (activo) {
				offsetX = target [next].localPosition.x - target [actual].localPosition.x;
				offsetZ = target [next].localPosition.z - target [actual].localPosition.z;
				root [next].localPosition = new Vector3 (root [next].localPosition.x - offsetX, root [next].localPosition.y, root [next].localPosition.z - offsetZ);
				panel.SetActive (false);
				//right.GetComponent<Renderer> ().sharedMaterial = materials [0];
				activo = false;
			}
			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Right")) {

				BasketBall [actual].SetActive (false);
				PlayerSkin [actual].sharedMesh = mesh [0];
				BasketBall [next].SetActive (true);
				PlayerSkin [next].sharedMesh = mesh [1];


				if (anim [actual].GetCurrentAnimatorStateInfo (0).IsName ("End_Right")) 
					anim [actual].SetTrigger ("Right_Reset"); 
			}
			else if (anim[next].GetCurrentAnimatorStateInfo (0).IsName ("End_Right")) {

				panel.transform.localPosition = new Vector3 ( target [next].localPosition.x+36, panel.transform.localPosition.y,  target [next].localPosition.z+5);


				if (actual == 1)
					actual = 0;
				else
					actual = 1;

				if (next == 1)
					next = 0;
				else
					next = 1;
				state = 0;
				GotoRight = false;
				changeIdle = true;

			}
			break;
		}
	}


	void toLeft (){
		switch (state) {

		case 0:
			changeAny = false;
			PlayerSkin [next].sharedMesh = mesh [0];
			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Idle")) 
				anim [next].SetTrigger ("Init_Left");

			state = 1;
			changeIdle = false;
			activo = true;
			break;

		case 1:

			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Init_Left")) {
				anim [next].SetTrigger ("Left");
				state = 2;
			} 
			break;
		case 2:
			if (activo) {
				offsetX = target [next].localPosition.x - target [actual].localPosition.x;
				offsetZ = target [next].localPosition.z - target [actual].localPosition.z;
				root [next].localPosition = new Vector3 (root [next].localPosition.x - offsetX, root [next].localPosition.y, root [next].localPosition.z - offsetZ);
				panel.SetActive (false);
				//left.GetComponent<Renderer> ().sharedMaterial = materials [0];

				activo = false;
			}
			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Left")) {

				BasketBall [actual].SetActive (false);
				PlayerSkin [actual].sharedMesh = mesh [0];
				BasketBall [next].SetActive (true);
				PlayerSkin [next].sharedMesh = mesh [1];



				if (anim [actual].GetCurrentAnimatorStateInfo (0).IsName ("End_Left")) 
					anim [actual].SetTrigger ("Left_Reset"); 
			}
			else if (anim[next].GetCurrentAnimatorStateInfo (0).IsName ("End_Left")) {

				panel.transform.localPosition = new Vector3 ( target [next].localPosition.x+36, panel.transform.localPosition.y,  target [next].localPosition.z+5);

				if (actual == 1)
					actual = 0;
				else
					actual = 1;

				if (next == 1)
					next = 0;
				else
					next = 1;
				state = 0;
				GotoLeft = false;
				changeIdle = true;

			}
			break;
		}
	}

	void toForward (){
		
	
		switch (state) {

		case 0:
			changeAny = false;
			PlayerSkin [next].sharedMesh = mesh [0];

			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Idle")) 
				anim [next].SetTrigger ("Init_Forward");

			state = 1;
			changeIdle = false;
			activo = true;
			break;

		case 1:

			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Init_Forward")) {
				anim [next].SetTrigger ("Forward");
				state = 2;
			} 
			break;
		case 2:
			if (activo) {
				offsetX = target [next].localPosition.x - target [actual].localPosition.x;
				offsetZ = target [next].localPosition.z - target [actual].localPosition.z;
				root [next].localPosition = new Vector3 (root [next].localPosition.x - offsetX, root [next].localPosition.y, root [next].localPosition.z - offsetZ);
				panel.SetActive (false);
				//fwd.GetComponent<Renderer> ().sharedMaterial = materials [0];
				activo = false;
			}
			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Forward")) {

				BasketBall [actual].SetActive (false);
				PlayerSkin [actual].sharedMesh = mesh [0];
				BasketBall [next].SetActive (true);
				PlayerSkin [next].sharedMesh = mesh [1];

				if (anim [actual].GetCurrentAnimatorStateInfo (0).IsName ("End_Forward")) 
					anim [actual].SetTrigger ("Forward_Reset"); 
			}
			else if (anim[next].GetCurrentAnimatorStateInfo (0).IsName ("End_Forward")) {
				
				panel.transform.localPosition = new Vector3 ( target [next].localPosition.x+36, panel.transform.localPosition.y,  target [next].localPosition.z+5);

				if (actual == 1)
					actual = 0;
				else
					actual = 1;

				if (next == 1)
					next = 0;
				else
					next = 1;
				state = 0;
				GotoForward = false;
				changeIdle = true;

			}
			break;
		}
		
	}


	void toBackward (){


		switch (state) {

		case 0:
			changeAny = false;
			PlayerSkin [next].sharedMesh = mesh [0];
			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Idle")) 
				anim [next].SetTrigger ("Init_Backward");

			state = 1;
			changeIdle = false;
			activo = true;
			break;

		case 1:

			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Init_Backward")) {
				anim [next].SetTrigger ("Backward");
				state = 2;
			} 
			break;
		case 2:
			if (activo) {
				offsetX = target [next].localPosition.x - target [actual].localPosition.x;
				offsetZ = target [next].localPosition.z - target [actual].localPosition.z;
				root [next].localPosition = new Vector3 (root [next].localPosition.x - offsetX, root [next].localPosition.y, root [next].localPosition.z - offsetZ);
				panel.SetActive (false);
				//bwd.GetComponent<Renderer> ().sharedMaterial = materials [0];
				activo = false;
			}if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Backward")) {

				BasketBall [actual].SetActive (false);
				PlayerSkin [actual].sharedMesh = mesh [0];
				BasketBall [next].SetActive (true);
				PlayerSkin [next].sharedMesh = mesh [1];
				if (anim [actual].GetCurrentAnimatorStateInfo (0).IsName ("End_Backward")) 
					anim [actual].SetTrigger ("Backward_Reset"); 
			}
			else if (anim[next].GetCurrentAnimatorStateInfo (0).IsName ("End_Backward")) {

				panel.transform.localPosition = new Vector3 ( target [next].localPosition.x+36, panel.transform.localPosition.y,  target [next].localPosition.z+5);

				if (actual == 1)
					actual = 0;
				else
					actual = 1;

				if (next == 1)
					next = 0;
				else
					next = 1;
				state = 0;
				GotoBackward = false;
				changeIdle = true;

			}
			break;
		}

	}


	void toIdle (){

		switch (state) {

		case 0:
			

			PlayerSkin [next].sharedMesh = mesh [0];
			anim[next].SetTrigger ("Idle_Reset");
			state = 1;
			break;

		case 1:
			
			if (anim [next].GetCurrentAnimatorStateInfo (0).IsName ("Idle")) {
				offsetX = target [next].localPosition.x - target [actual].localPosition.x;
				offsetZ = target [next].localPosition.z - target [actual].localPosition.z;

				root [next].localPosition = new Vector3 (root [next].localPosition.x - offsetX, root [next].localPosition.y, root [next].localPosition.z - offsetZ);
				state = 2;
			} 
			break;
		
		case 2:
			
			BasketBall [actual].SetActive (false);
			PlayerSkin [actual].sharedMesh = mesh [0];
			BasketBall [next].SetActive (true);
			PlayerSkin [next].sharedMesh = mesh [1];

			anim [actual].SetTrigger ("Idle_Reset");
			state = 3;
			break;

		case 3:
			if (anim [actual].GetCurrentAnimatorStateInfo (0).IsName ("Idle")) {
				
				if (actual == 1)
					actual = 0;
				else
					actual = 1;

				if (next == 1)
					next = 0;
				else
					next = 1;
				
				state = 0;
				changeAny = true;
				changeIdle = false;
				panel.SetActive (true);
			}
			break;
		}
	}

	public void EndAnimation(){
		//coin.GetComponent<Renderer> ().material.color = Color.white;
		character.transform.SetParent (models);
		character.SetActive (true);
		players.transform.SetParent (temp);
		players.SetActive (false);
		playerController.SetActive (false);
		changeAny = true;
		if(!targetFound){
			OnTrackingInActive ();
		}
	}

	public void closeTutorial(){
		//tutorial.SetActive (false);
		AR.SetActive (true);
	}
		
	public void closeMenu(){
		//tutorial1.SetActive (false);
		//tutorial2.SetActive (false);
		//tutorial3.SetActive (false);
		//tutorial4.SetActive (false);
		//closeButton3.SetActive (false);
	}

	public void menu1Tutorial(){
		//tutorial1.SetActive (true);
		//closeButton3.SetActive (true);
	}

	public void menu2Tutorial(){
		//tutorial2.SetActive (true);
		//closeButton3.SetActive (true);
	}

	public void menu3Tutorial(){
		//tutorial3.SetActive (true);
		//closeButton3.SetActive (true);
	}

	public void menu4Tutorial(){
		//tutorial4.SetActive (true);
		//closeButton3.SetActive (true);
	}

	public void playSound(int num){
		if(num < sounds.Length)
			audio.PlayOneShot (sounds[num]);
	}

	public void PausePlayAnimation(){
		isPause = !isPause;
		if (isPause) {
			animation2.speed = 0;
			btnPlay.SetActive (true);
			btnPause.SetActive (false);
		} else {
			animation2.speed = 1;
			btnPlay.SetActive (false);
			btnPause.SetActive (true);
		}
	}


	public void ffAnimation(){
		if (animation2.speed < 2) {
			animation2.speed += 0.20f;
			btnPlay.SetActive (true);
			btnPause.SetActive (false);
		}
	}

	private void OnTrackingActive()
	{
		
		Renderer[] rendererComponents = players.GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = players.GetComponentsInChildren<Collider>(true);


		// Enable rendering:
		foreach (Renderer component in rendererComponents) {

			component.enabled = true;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents) {
			component.enabled = true;
		}


	}

	private void OnTrackingInActive()
	{

		Renderer[] rendererComponents = character.GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = character.GetComponentsInChildren<Collider>(true);


		// Enable rendering:
		foreach (Renderer component in rendererComponents) {

			component.enabled = false;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents) {
			component.enabled = false;
		}


	}

	public void changeImage(){
		

		if (pagesActual > 5) {
			pagesActual = 0;
		}

		if (pagesActual <= 0) {
			btnsIntro2.SetActive (false);
			pagesActual = 0;
		}

		for (int i = 1; i < introPages.Length; i++) {
			if(pagesActual==i)
				introPages [i].SetActive (true);
			else
				introPages [i].SetActive (false);
		}
	}


	public void nextImage(){
		pagesActual++;
		changeImage ();
		btnsIntro2.SetActive (true);
	}

	public void prevImage(){
		pagesActual--;
		changeImage ();
	}


	public void exitIntro(){
		for (int i = 0; i < introPages.Length; i++) {
			introPages [i].SetActive (false);
		}
		bgIntro.SetActive (false);
		btnsIntro1.SetActive (false);
		btnsIntro2.SetActive (false);
		btnsIntro3.SetActive (false);
		btnHelp.SetActive (true);
		btnLink.SetActive (false);

	}

	public void helpIntro(){
		introPages [0].SetActive (true);
		pagesActual = 0;
		for (int i = 1; i < introPages.Length; i++) {
			introPages [i].SetActive (false);
		}
		bgIntro.SetActive (true);
		btnsIntro1.SetActive (true);
		btnsIntro2.SetActive (false);
		btnsIntro3.SetActive (true);
		btnHelp.SetActive (false);
		btnLink.SetActive (true);
	}

	public void downloadTarget(){
		Application.OpenURL("http://eac-ualr.org/TrojansAR.jpg");
	}
}
