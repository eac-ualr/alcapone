﻿using UnityEngine;
using System.Collections;

public class TriggerAnimation : MonoBehaviour {

	Animator animator;

	void Start () {
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Ended ) {
			animator.SetTrigger ("Play");

		}
		if(Input.GetKey(KeyCode.A)){
			animator.SetTrigger ("Left");
		}
		if(Input.GetKey(KeyCode.D)){
			animator.SetTrigger ("Right");
		}
	}
}


