﻿using UnityEngine;
using System.Collections;

public class ControlCollider4 : MonoBehaviour {

    public SwipeThrow swipeThrow;
	
	void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            swipeThrow.goal = 5;
			swipeThrow.playSound(Random.Range(10,12));
        }

    }
}
