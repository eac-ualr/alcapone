﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class scrollScreen : MonoBehaviour {

	public float speed = 0.1f;
	private bool clave=false;
	private bool timerActive = false;
	private float timerCount;
	private float actualValue;
	private int state;
	public Text texto;
	public Text texto1;


	Animator animator;

	void Start () {
		animator = GetComponent<Animator> ();
		actualValue = 0;
		state = 0;
	}

	void Update () {
		/*
		if (Input.touchCount >= 2 && !timerActive) {
			clave = !clave;
			timerActive = true;
			timerCount = Time.time;
		}

		if (timerActive && Time.time > timerCount + 1) {
			timerActive = false;
		}
		*/
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) {
			Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;
			texto.text = touchDeltaPosition.y.ToString ();
			if (touchDeltaPosition.y > 0.5f) 
				state = 1;
			else if (touchDeltaPosition.y < -0.5f)
				state = 2;


			//transform.Translate (touchDeltaPosition.x * speed, 0, touchDeltaPosition.y * speed);

		} 
		else if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Ended) {
			state = 0;
		}
			


		switch (state) {
		case 0:
			actualValue = Mathf.Lerp (actualValue, 0, 0.1f);
			break;
		case 1:
			actualValue = Mathf.Lerp (actualValue, -1, 0.1f);
			break;
		case 2:
			actualValue = Mathf.Lerp (actualValue, 1, 0.1f);
			break;
		
		}


		animator.SetFloat ("Blend", actualValue);
		texto1.text = state.ToString ();
	}
}


